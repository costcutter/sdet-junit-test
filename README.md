# sdet-junit-test

### Instructions

Check out and study the Java project.

Write a JUnit test suite to test the implementation of method PracticalTestService.findOrdersSince(LocalDate date).  It is up to you to identify the various use cases and conditions etc.

An example test class is already included to get you started.

JUnit and Mockito libraries are included for your convenience.  If you wish to include further dependencies e.g. Hamcrest, AssertJ etc. please do so by amending the POM file.

When you have completed the test, please submit your code either as a ZIP archive or provide a link to the checked in source in your personal Github or similar.



